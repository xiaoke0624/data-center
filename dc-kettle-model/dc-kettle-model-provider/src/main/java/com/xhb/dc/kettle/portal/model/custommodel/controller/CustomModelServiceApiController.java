package com.xhb.dc.kettle.portal.model.custommodel.controller;

import com.xhb.dc.kettle.portal.model.custommodel.api.CustomModelServiceApi;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>****************************************************************************</p>
 * <ul style="margin:15px;">
 * <li>Description : description</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2020/2/10 4:25 PM</li>
 * <li>Author      : ksice_xt</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RequestMapping("/custom")
public class CustomModelServiceApiController implements CustomModelServiceApi {

}
