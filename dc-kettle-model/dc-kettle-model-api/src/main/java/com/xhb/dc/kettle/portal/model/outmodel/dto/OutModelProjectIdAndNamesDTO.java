package com.xhb.dc.kettle.portal.model.outmodel.dto;

import lombok.Data;

/**
 * OutModelProjectIdAndNamesDTO.
 */
@Data
public class OutModelProjectIdAndNamesDTO {

    private String projectId;

    private String projectName;

}
